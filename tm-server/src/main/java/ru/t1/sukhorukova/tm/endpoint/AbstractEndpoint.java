package ru.t1.sukhorukova.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;
import ru.t1.sukhorukova.tm.api.service.IUserService;
import ru.t1.sukhorukova.tm.dto.request.AbstractUserRequest;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.exception.user.NotLoggedInException;
import ru.t1.sukhorukova.tm.exception.user.PermissionException;
import ru.t1.sukhorukova.tm.model.User;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final ILocatorService locatorService;

    public AbstractEndpoint(final ILocatorService locatorService) {
        this.locatorService = locatorService;
    }

    protected void check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null || role == null) throw new PermissionException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new PermissionException();
        @NotNull final ILocatorService locatorService = getLocatorService();
        @NotNull final IUserService userService = locatorService.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new PermissionException();
        @Nullable final Role roleUser = user.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new PermissionException();
    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new PermissionException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new PermissionException();
    }

}
