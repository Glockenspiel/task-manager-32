package ru.t1.sukhorukova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;
import ru.t1.sukhorukova.tm.api.service.IProjectService;
import ru.t1.sukhorukova.tm.api.service.IProjectTaskService;
import ru.t1.sukhorukova.tm.dto.request.project.*;
import ru.t1.sukhorukova.tm.dto.response.project.*;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Status status = request.getStatus();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusById(userId, projectId, status);

        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusByIndex(userId, index, status);

        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectClearResponse clear(
            @NotNull final ProjectClearRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();

        getLocatorService().getProjectService().removeAll(userId);

        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse completeById(
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusById(userId, projectId, Status.COMPLETED);

        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse completeByIndex(
            @NotNull final ProjectCompleteByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);

        return new ProjectCompleteByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectCreateResponse create(
            @NotNull final ProjectCreateRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Project project = getLocatorService().getProjectService().create(userId, name, description);

        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    public ProjectListResponse list(
            @NotNull final ProjectListRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final ProjectSort sort = request.getSort();

        @Nullable final Comparator<Project> comparator = sort != null ? sort.getComparator() : null;
        @Nullable final List<Project> projects = getLocatorService().getProjectService().findAll(userId, comparator);

        @NotNull final ProjectListResponse response = new ProjectListResponse();
        response.setProjects(projects);
        return response;
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeById(
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();

        getLocatorService().getProjectTaskService().removeProjectById(userId, projectId);

        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeByIndex(
            @NotNull final ProjectRemoveByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Project project = getLocatorService().getProjectService().findOneByIndex(userId, index);
        getLocatorService().getProjectTaskService().removeProjectById(userId, project.getId());

        return new ProjectRemoveByIndexResponse();
    }

    @NotNull
    @Override
    public ProjectShowByIdResponse showById(
            @NotNull final ProjectShowByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();

        @Nullable final Project project = getLocatorService().getProjectService().findOneById(userId, projectId);

        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectShowByIndexResponse showByIndex(
            @NotNull final ProjectShowByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Project project = getLocatorService().getProjectService().findOneByIndex(userId, index);

        return new ProjectShowByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse startById(
            @NotNull final ProjectStartByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusById(userId, projectId, Status.IN_PROGRESS);

        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse startByIndex(
            @NotNull final ProjectStartByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Project project = getLocatorService().getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);

        return new ProjectStartByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateById(
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Project project = getLocatorService().getProjectService().updateById(userId, projectId, name, description);

        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateByIndex(
            @NotNull final ProjectUpdateByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Project project = getLocatorService().getProjectService().updateByIndex(userId, index, name, description);

        return new ProjectUpdateByIndexResponse(project);
    }

}
