package ru.t1.sukhorukova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.endpoint.*;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.api.service.*;
import ru.t1.sukhorukova.tm.dto.request.system.*;
import ru.t1.sukhorukova.tm.dto.request.data.*;
import ru.t1.sukhorukova.tm.dto.request.project.*;
import ru.t1.sukhorukova.tm.dto.request.task.*;
import ru.t1.sukhorukova.tm.dto.request.user.*;
import ru.t1.sukhorukova.tm.endpoint.*;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.model.User;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.sukhorukova.tm.repository.UserRepository;
import ru.t1.sukhorukova.tm.service.*;
import ru.t1.sukhorukova.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements ILocatorService {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ApplicationAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ApplicationVersionRequest.class, systemEndpoint::getVersion);
        server.registry(SystemInfoRequest.class, systemEndpoint::getSystemInfo);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::backupLoad);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::backupSave);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::base64Load);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::base64Save);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::binaryLoad);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::binarySave);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::jsonLoadFasterXml);
        server.registry(DataJsonLoadJaxBRequest.class, domainEndpoint::jsonLoadJaxB);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::jsonSaveFasterXml);
        server.registry(DataJsonSaveJaxBRequest.class, domainEndpoint::jsonSaveJaxB);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::xmlLoadFasterXml);
        server.registry(DataXmlLoadJaxBRequest.class, domainEndpoint::xmlLoadJaxB);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::xmlSaveFasterXml);
        server.registry(DataXmlSaveJaxBRequest.class, domainEndpoint::xmlSaveJaxB);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::yamlLoadFasterXml);
        server.registry(DataYamlSaveFasterXmlRequest.class, domainEndpoint::yamlSaveFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clear);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::create);
        server.registry(ProjectListRequest.class, projectEndpoint::list);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateByIndex);

        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindToProject);
        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clear);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::create);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::listByProjectId);
        server.registry(TaskListRequest.class, taskEndpoint::list);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showByIndex);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startByIndex);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindFromProject);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateByIndex);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);
        server.registry(UserLockRequest.class, userEndpoint::lock);
        server.registry(UserRegistryRequest.class, userEndpoint::registry);
        server.registry(UserRemoveRequest.class, userEndpoint::remove);
        server.registry(UserUnlockRequest.class, userEndpoint::unlock);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfile);
        server.registry(UserViewProfileRequest.class, userEndpoint::viewProfile);
    }

    public void start(@NotNull final String[] args) {
        initPID();
        initLogger();
        initDemoData();
        initBackup();
        initServer();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull User user1 = userService.create("USER_01", "user01", "user01@address.ru");
        @NotNull User user2 = userService.create("USER_02", "user02", "user02@address.ru");
        @NotNull User user3 = userService.create("USER_03", "user03", "user03@address.ru");

        projectService.add(new Project(user1, "PROJECT_01", "Test project 1", Status.COMPLETED));
        projectService.add(new Project(user1, "PROJECT_18", "Test project 18", Status.IN_PROGRESS));
        projectService.add(new Project(user2, "PROJECT_02", "Test project 2", Status.NOT_STARTED));
        projectService.add(new Project(user3, "PROJECT_26", "Test project 26", Status.IN_PROGRESS));

        taskService.add(new Task(user1, "TASK_01", "Test task 1", Status.COMPLETED));
        taskService.add(new Task(user1, "TASK_18", "Test task 18", Status.IN_PROGRESS));
        taskService.add(new Task(user1, "TASK_02", "Test task 2", Status.NOT_STARTED));
        taskService.add(new Task(user2, "TASK_26", "Test task 26", Status.IN_PROGRESS));
    }

    private void initBackup() {
        backup.init();
    }

    private void initServer() {
        server.start();
    }

}
