package ru.t1.sukhorukova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.endpoint.IUserEndpoint;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;
import ru.t1.sukhorukova.tm.api.service.IUserService;
import ru.t1.sukhorukova.tm.dto.request.user.*;
import ru.t1.sukhorukova.tm.dto.response.user.*;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.model.User;

public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(
            @NotNull final UserChangePasswordRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String password = request.getPassword();

        @Nullable final User user = getLocatorService().getUserService().setPassword(userId, password);

        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    public UserLockResponse lock(
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();

        @Nullable final User user = getLocatorService().getUserService().lockOneByLogin(login);

        return new UserLockResponse(user);
    }

    @NotNull
    @Override
    public UserRegistryResponse registry(
            @NotNull final UserRegistryRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();

        @Nullable final User user = getLocatorService().getUserService().create(login, password, email);

        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveResponse remove(
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();

        @Nullable final User user = getLocatorService().getUserService().removeOneByLogin(login);

        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    public UserUnlockResponse unlock(
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();

        @Nullable final User user = getLocatorService().getUserService().unlockOneByLogin(login);

        return new UserUnlockResponse(user);
    }

    @NotNull
    @Override
    public UserUpdateProfileResponse updateProfile(
            @NotNull final UserUpdateProfileRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();

        @Nullable final User user = getLocatorService().getUserService().updateUser(userId, firstName, lastName, middleName);

        return new UserUpdateProfileResponse(user);
    }

    @NotNull
    @Override
    public UserViewProfileResponse viewProfile(
            @NotNull final UserViewProfileRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();

        @Nullable final User user = getLocatorService().getUserService().findOneById(userId);

        return new UserViewProfileResponse(user);
    }

}
