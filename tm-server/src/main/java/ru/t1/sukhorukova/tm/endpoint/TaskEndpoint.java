package ru.t1.sukhorukova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;
import ru.t1.sukhorukova.tm.api.service.IProjectTaskService;
import ru.t1.sukhorukova.tm.api.service.ITaskService;
import ru.t1.sukhorukova.tm.dto.request.task.*;
import ru.t1.sukhorukova.tm.dto.response.task.*;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindToProject(
            @NotNull final TaskBindToProjectRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();

        getLocatorService().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);

        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeStatusById(
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final Status status = request.getStatus();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusById(userId, taskId, status);

        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIndexResponse changeStatusByIndex(
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusByIndex(userId, index, status);

        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskClearResponse clear(
            @NotNull final TaskClearRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();

        getLocatorService().getTaskService().removeAll(userId);

        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCompleteByIdResponse completeById(
            @NotNull final TaskCompleteByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusById(userId, taskId, Status.COMPLETED);

        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskCompleteByIndexResponse completeByIndex(
            @NotNull final TaskCompleteByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusByIndex(userId, index, Status.COMPLETED);

        return new TaskCompleteByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskCreateResponse create(
            @NotNull final TaskCreateRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Task task = getLocatorService().getTaskService().create(userId, name, description);

        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    public TaskListByProjectIdResponse listByProjectId(
            @NotNull final TaskListByProjectIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();

        @NotNull final List<Task> tasks = getLocatorService().getTaskService().findAllByProjectId(userId, projectId);

        @NotNull final TaskListByProjectIdResponse response = new TaskListByProjectIdResponse();
        response.setTasks(tasks);
        return response;
    }

    @NotNull
    @Override
    public TaskListResponse list(
            @NotNull final TaskListRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final TaskSort sort = request.getSort();

        @Nullable final Comparator<Task> comparator = sort != null ? sort.getComparator() : null;
        @NotNull final List<Task> tasks = getLocatorService().getTaskService().findAll(userId, comparator);

        @NotNull final TaskListResponse response = new TaskListResponse();
        response.setTasks(tasks);
        return response;
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeById(
            @NotNull final TaskRemoveByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();

        @Nullable final Task task = getLocatorService().getTaskService().removeOneById(userId, taskId);

        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskRemoveByIndexResponse removeByIndex(
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Task task = getLocatorService().getTaskService().removeOneByIndex(userId, index);

        return new TaskRemoveByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskShowByIdResponse showById(
            @NotNull final TaskShowByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();

        @Nullable final Task task = getLocatorService().getTaskService().findOneById(userId, taskId);

        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskShowByIndexResponse showByIndex(
            @NotNull final TaskShowByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Task task = getLocatorService().getTaskService().findOneByIndex(userId, index);

        return new TaskShowByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskStartByIdResponse startById(
            @NotNull final TaskStartByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusById(userId, taskId, Status.IN_PROGRESS);

        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskStartByIndexResponse startByIndex(
            @NotNull final TaskStartByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();

        @Nullable final Task task = getLocatorService().getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);

        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindFromProject(
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();

        getLocatorService().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);

        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateById(
            @NotNull final TaskUpdateByIdRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Task task = getLocatorService().getTaskService().updateById(userId, taskId, name, description);

        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    public TaskUpdateByIndexResponse updateByIndex(
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();

        @Nullable final Task task = getLocatorService().getTaskService().updateByIndex(userId, index, name, description);

        return new TaskUpdateByIndexResponse(task);
    }

}
