package ru.t1.sukhorukova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;
import ru.t1.sukhorukova.tm.api.service.IPropertyService;
import ru.t1.sukhorukova.tm.dto.request.system.*;
import ru.t1.sukhorukova.tm.dto.response.system.*;
import ru.t1.sukhorukova.tm.util.FormatUtil;

public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull
    @Override
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        @NotNull final IPropertyService propertyService = getLocatorService().getPropertyService();

        @NotNull final ApplicationAboutResponse response = new ApplicationAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    public ApplicationVersionResponse getVersion(@NotNull final ApplicationVersionRequest request) {
        @NotNull final IPropertyService propertyService = getLocatorService().getPropertyService();

        @NotNull final ApplicationVersionResponse response = new ApplicationVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @NotNull
    @Override
    public SystemInfoResponse getSystemInfo(@NotNull final SystemInfoRequest request) {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        @NotNull final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        @NotNull final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        @NotNull final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        @NotNull final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        @NotNull final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;

        @NotNull final SystemInfoResponse response = new SystemInfoResponse();
        response.setAvailableProcessors(availableProcessors);
        response.setFreeMemoryFormat(freeMemoryFormat);
        response.setMaxMemoryValue(maxMemoryValue);
        response.setTotalMemoryFormat(totalMemoryFormat);
        response.setUsageMemoryFormat(usageMemoryFormat);
        return response;
    }

}
