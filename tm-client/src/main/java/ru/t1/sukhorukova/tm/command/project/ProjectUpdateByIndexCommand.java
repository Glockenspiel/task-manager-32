package ru.t1.sukhorukova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.project.ProjectUpdateByIndexRequest;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-update-by-index";

    @NotNull
    public static final String DESCRIPTION = "Update project by index.";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("Enter project name:");
        @Nullable final String name = TerminalUtil.nextLine();

        System.out.println("Enter project description:");
        @Nullable final String description = TerminalUtil.nextLine();

        @Nullable final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest();
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        getProjectEndpoint().updateByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
