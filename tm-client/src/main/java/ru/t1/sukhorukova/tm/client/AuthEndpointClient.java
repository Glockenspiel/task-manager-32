package ru.t1.sukhorukova.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.user.UserLoginRequest;
import ru.t1.sukhorukova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sukhorukova.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.sukhorukova.tm.dto.response.user.UserLoginResponse;
import ru.t1.sukhorukova.tm.dto.response.user.UserLogoutResponse;
import ru.t1.sukhorukova.tm.dto.response.user.UserViewProfileResponse;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements ru.t1.sukhorukova.tm.api.endpoint.IAuthEndpoint {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserViewProfileResponse viewProfile(@NotNull UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final AuthEndpointClient authEndpointClient = new AuthEndpointClient();
        authEndpointClient.connect();
        System.out.println(authEndpointClient.viewProfile(new UserViewProfileRequest()).getUser());
        System.out.println(authEndpointClient.login(new UserLoginRequest("123321", "123321")).getSuccess());

        System.out.println(authEndpointClient.login(new UserLoginRequest("USER_01", "user01")).getSuccess());
        System.out.println(authEndpointClient.viewProfile(new UserViewProfileRequest()).getUser().getEmail());
        System.out.println(authEndpointClient.logout(new UserLogoutRequest()));
        System.out.println(authEndpointClient.viewProfile(new UserViewProfileRequest()).getUser());
        authEndpointClient.disconnect();
    }

}
