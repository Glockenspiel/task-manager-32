package ru.t1.sukhorukova.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.endpoint.ISystemEndpoint;
import ru.t1.sukhorukova.tm.dto.request.system.*;
import ru.t1.sukhorukova.tm.dto.response.system.*;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    public SystemEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SystemInfoResponse getSystemInfo(@NotNull final SystemInfoRequest request) {
        return call(request, SystemInfoResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ApplicationAboutResponse applicationAboutResponse = client.getAbout(new ApplicationAboutRequest());
        System.out.println(applicationAboutResponse.getEmail());
        System.out.println(applicationAboutResponse.getName());
        @NotNull final ApplicationVersionResponse applicationVersionResponse = client.getVersion(new ApplicationVersionRequest());
        System.out.println(applicationVersionResponse.getVersion());
        client.disconnect();
    }

}
