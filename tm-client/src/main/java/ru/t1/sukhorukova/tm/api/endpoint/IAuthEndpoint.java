package ru.t1.sukhorukova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.user.UserLoginRequest;
import ru.t1.sukhorukova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.sukhorukova.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.sukhorukova.tm.dto.response.user.UserLoginResponse;
import ru.t1.sukhorukova.tm.dto.response.user.UserLogoutResponse;
import ru.t1.sukhorukova.tm.dto.response.user.UserViewProfileResponse;

public interface IAuthEndpoint extends IEndpointClient {

    UserLoginResponse login(@NotNull UserLoginRequest request);

    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    UserViewProfileResponse viewProfile(@NotNull UserViewProfileRequest request);

}
