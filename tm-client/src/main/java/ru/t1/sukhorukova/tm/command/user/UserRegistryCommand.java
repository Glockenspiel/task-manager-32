package ru.t1.sukhorukova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.user.UserRegistryRequest;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-registry";
    @NotNull private final String DESCRIPTION = "Registry user.";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");

        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();

        System.out.println("Enter password:");
        @Nullable final String password = TerminalUtil.nextLine();

        System.out.println("Enter email:");
        @Nullable final String email = TerminalUtil.nextLine();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        getUserEndpoint().registry(request);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable @Override
    public Role[] getRoles() {
        return null;
    }

}
