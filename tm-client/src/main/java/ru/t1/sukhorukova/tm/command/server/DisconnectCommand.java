package ru.t1.sukhorukova.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.endpoint.IEndpointClient;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;

import java.net.Socket;

public final class DisconnectCommand extends AbstractServerCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @Override
    @SneakyThrows
    public void execute() {
        try {
            getLocatorService().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            getLocatorService().getLoggerService().error(e);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
