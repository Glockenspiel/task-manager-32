package ru.t1.sukhorukova.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.endpoint.IDomainEndpoint;
import ru.t1.sukhorukova.tm.dto.request.data.*;
import ru.t1.sukhorukova.tm.dto.response.data.*;

@NoArgsConstructor
public final class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpoint {

    public DomainEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupLoadResponse backupLoad(@NotNull DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBackupSaveResponse backupSave(@NotNull DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64LoadResponse base64Load(@NotNull DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBase64SaveResponse base64Save(@NotNull DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinaryLoadResponse binaryLoad(@NotNull DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataBinarySaveResponse binarySave(@NotNull DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonLoadFasterXmlResponse jsonLoadFasterXml(@NotNull DataJsonLoadFasterXmlRequest request) {
        return call(request, DataJsonLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonLoadJaxBResponse jsonLoadJaxB(@NotNull DataJsonLoadJaxBRequest request) {
        return call(request, DataJsonLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonSaveFasterXmlResponse jsonSaveFasterXml(@NotNull DataJsonSaveFasterXmlRequest request) {
        return call(request, DataJsonSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataJsonSaveJaxBResponse jsonSaveJaxB(@NotNull DataJsonSaveJaxBRequest request) {
        return call(request, DataJsonSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlLoadFasterXmlResponse xmlLoadFasterXml(@NotNull DataXmlLoadFasterXmlRequest request) {
        return call(request, DataXmlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlLoadJaxBResponse xmlLoadJaxB(@NotNull DataXmlLoadJaxBRequest request) {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlSaveFasterXmlResponse xmlSaveFasterXml(@NotNull DataXmlSaveFasterXmlRequest request) {
        return call(request, DataXmlSaveFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataXmlSaveJaxBResponse xmlSaveJaxB(@NotNull DataXmlSaveJaxBRequest request) {
        return call(request, DataXmlSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlLoadFasterXmlResponse yamlLoadFasterXml(@NotNull DataYamlLoadFasterXmlRequest request) {
        return call(request, DataYamlLoadFasterXmlResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public DataYamlSaveFasterXmlResponse yamlSaveFasterXml(@NotNull DataYamlSaveFasterXmlRequest request) {
        return call(request, DataYamlSaveFasterXmlResponse.class);
    }

}
