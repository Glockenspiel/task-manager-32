package ru.t1.sukhorukova.tm.command.server;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.command.AbstractCommand;
import ru.t1.sukhorukova.tm.enumerated.Role;

public abstract class AbstractServerCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
