package ru.t1.sukhorukova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.sukhorukova.tm.api.endpoint.*;
import ru.t1.sukhorukova.tm.api.repository.ICommandRepository;
import ru.t1.sukhorukova.tm.api.service.*;
import ru.t1.sukhorukova.tm.client.*;
import ru.t1.sukhorukova.tm.command.AbstractCommand;
import ru.t1.sukhorukova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.sukhorukova.tm.exception.system.CommandNotSupportedException;
import ru.t1.sukhorukova.tm.repository.CommandRepository;
import ru.t1.sukhorukova.tm.service.*;
import ru.t1.sukhorukova.tm.util.SystemUtil;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements ILocatorService {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.sukhorukova.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final IEndpointClient connectionEndpointClient = new ConnectionEndpointClient();

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpointClient = new AuthEndpointClient();

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpointClient = new SystemEndpointClient();

    @Getter
    @NotNull
    private final IDomainEndpoint domainEndpointClient = new DomainEndpointClient();

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpointClient = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpointClient = new TaskEndpointClient();

    @Getter
    @NotNull
    private final IUserEndpoint userEndpointClient = new UserEndpointClient();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes)
            registry(clazz);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setLocatorService(this);
        commandService.add(command);
    }

    public void start(@NotNull final String[] args) {
        initPID();
        initLogger();
        processArguments(args);
        initFileScanner();
        connect();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.out.println("[OK]");
            System.exit(0);
        } catch (@NotNull final RuntimeException e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initFileScanner() {
        fileScanner.init();
    }

    private void connect() {
        try {
            processCommand("connect");
        } catch (@NotNull final RuntimeException e) {
            loggerService.error(e);
        }
    }

}
