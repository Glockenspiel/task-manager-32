package ru.t1.sukhorukova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.Domain;
import ru.t1.sukhorukova.tm.dto.request.data.DataYamlSaveFasterXmlRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-yaml";

    @NotNull
    public static final String DESCRIPTION = "Save data in yaml file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE YAML]");

        getDomainEndpoint().yamlSaveFasterXml(new DataYamlSaveFasterXmlRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
