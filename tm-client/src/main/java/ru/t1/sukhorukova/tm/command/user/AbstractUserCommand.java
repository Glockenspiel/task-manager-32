package ru.t1.sukhorukova.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sukhorukova.tm.api.endpoint.IUserEndpoint;
import ru.t1.sukhorukova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected IUserEndpoint getUserEndpoint() {
        return getLocatorService().getUserEndpointClient();
    }

    protected IAuthEndpoint getAuthEndpoint() {
        return getLocatorService().getAuthEndpointClient();
    }

    @Nullable @Override
    public String getArgument() {
        return null;
    }

}
