package ru.t1.sukhorukova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.data.*;
import ru.t1.sukhorukova.tm.dto.response.data.*;

public interface IDomainEndpoint extends IEndpointClient {

    @NotNull
    DataBackupLoadResponse backupLoad(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse backupSave(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse base64Load(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse base64Save(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse binaryLoad(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse binarySave(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse jsonLoadFasterXml(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxBResponse jsonLoadJaxB(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse jsonSaveFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonSaveJaxBResponse jsonSaveJaxB(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse xmlLoadFasterXml(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxBResponse xmlLoadJaxB(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse xmlSaveFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlSaveJaxBResponse xmlSaveJaxB(@NotNull DataXmlSaveJaxBRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse yamlLoadFasterXml(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse yamlSaveFasterXml(@NotNull DataYamlSaveFasterXmlRequest request);

}
