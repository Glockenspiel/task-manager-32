package ru.t1.sukhorukova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.user.UserRemoveRequest;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull private final String NAME = "user-remove";
    @NotNull private final String DESCRIPTION = "User remove.";

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");

        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();

        @NotNull final UserRemoveRequest request = new UserRemoveRequest();
        request.setLogin(login);
        getUserEndpoint().remove(request);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
