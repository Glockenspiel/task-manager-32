package ru.t1.sukhorukova.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.endpoint.IEndpointClient;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;

import java.net.Socket;

public final class ConnectCommand extends AbstractServerCommand {

    @NotNull
    public static final String NAME = "connect";

    @Override
    @SneakyThrows
    public void execute() {
        try {
            @NotNull final ILocatorService locatorService = getLocatorService();
            @NotNull final IEndpointClient endpointClient = locatorService.getConnectionEndpointClient();
            @Nullable final Socket socket = endpointClient.connect();
            locatorService.getAuthEndpointClient().setSocket(socket);
            locatorService.getSystemEndpointClient().setSocket(socket);
            locatorService.getDomainEndpointClient().setSocket(socket);
            locatorService.getProjectEndpointClient().setSocket(socket);
            locatorService.getTaskEndpointClient().setSocket(socket);
            locatorService.getUserEndpointClient().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getLocatorService().getLoggerService().error(e);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
