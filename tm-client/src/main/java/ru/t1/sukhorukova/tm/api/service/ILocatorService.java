package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.endpoint.*;

public interface ILocatorService {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    IAuthEndpoint getAuthEndpointClient();

    @NotNull
    ISystemEndpoint getSystemEndpointClient();

    @NotNull
    IDomainEndpoint getDomainEndpointClient();

    @NotNull
    IProjectEndpoint getProjectEndpointClient();

    @NotNull
    ITaskEndpoint getTaskEndpointClient();

    @NotNull
    IUserEndpoint getUserEndpointClient();

}
