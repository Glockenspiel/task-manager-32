package ru.t1.sukhorukova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @Override
    public void execute() {
        System.out.println("BIND TASK TO PROJECT");

        System.out.println("Enter project id:");
        @Nullable final String projectId = TerminalUtil.nextLine();

        System.out.println("Enter task id:");
        @Nullable final String taskId = TerminalUtil.nextLine();

        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest();
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        getTaskEndpoint().bindToProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
