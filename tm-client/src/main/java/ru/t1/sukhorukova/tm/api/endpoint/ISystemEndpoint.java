package ru.t1.sukhorukova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.system.*;
import ru.t1.sukhorukova.tm.dto.response.system.*;

public interface ISystemEndpoint extends IEndpointClient {

    @NotNull
    ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request);

    @NotNull
    ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request);

    @NotNull
    SystemInfoResponse getSystemInfo(@NotNull final SystemInfoRequest request);

}
