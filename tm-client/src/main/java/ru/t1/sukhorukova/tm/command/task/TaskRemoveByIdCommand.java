package ru.t1.sukhorukova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-remove-by-id";

    @NotNull
    public static final String DESCRIPTION = "Remove task by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");

        System.out.println("Enter task id:");
        @Nullable final String taskId = TerminalUtil.nextLine();

        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest();
        request.setTaskId(taskId);
        getTaskEndpoint().removeById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
