package ru.t1.sukhorukova.tm.dto.response.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Project;

import java.util.List;

@Getter
@Setter
public final class ProjectListResponse extends AbstractProjectResponse {

    @Nullable
    private List<Project> projects;

}
