package ru.t1.sukhorukova.tm.dto.response.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Task;

import java.util.List;

@Getter
@Setter
public final class TaskListResponse extends AbstractTaskResponse {

    @Nullable
    private List<Task> tasks;

}
