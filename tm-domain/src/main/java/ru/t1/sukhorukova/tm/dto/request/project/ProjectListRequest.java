package ru.t1.sukhorukova.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.AbstractUserRequest;
import ru.t1.sukhorukova.tm.enumerated.ProjectSort;
import ru.t1.sukhorukova.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ProjectSort sort;

}
