package ru.t1.sukhorukova.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.User;

public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable User user) {
        super(user);
    }

}
