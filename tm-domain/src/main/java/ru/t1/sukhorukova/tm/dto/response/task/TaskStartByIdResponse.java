package ru.t1.sukhorukova.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Task;

public final class TaskStartByIdResponse extends AbstractTaskResponse {

    public TaskStartByIdResponse(@Nullable Task task) {
        super(task);
    }

}
