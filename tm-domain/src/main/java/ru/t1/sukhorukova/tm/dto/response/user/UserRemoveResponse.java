package ru.t1.sukhorukova.tm.dto.response.user;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.User;

public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable User user) {
        super(user);
    }

}
