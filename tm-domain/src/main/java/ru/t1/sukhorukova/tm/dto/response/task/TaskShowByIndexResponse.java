package ru.t1.sukhorukova.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Task;

public final class TaskShowByIndexResponse extends AbstractTaskResponse {

    public TaskShowByIndexResponse(@Nullable Task task) {
        super(task);
    }

}
