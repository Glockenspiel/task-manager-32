package ru.t1.sukhorukova.tm.dto.response.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Task;

public final class TaskShowByIdResponse extends AbstractTaskResponse {

    public TaskShowByIdResponse(@Nullable Task task) {
        super(task);
    }

}
