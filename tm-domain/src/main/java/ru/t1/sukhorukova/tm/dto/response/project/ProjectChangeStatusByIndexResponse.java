package ru.t1.sukhorukova.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Project;

public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable Project project) {
        super(project);
    }

}
