package ru.t1.sukhorukova.tm.dto.response.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.Project;

public final class ProjectShowByIndexResponse extends AbstractProjectResponse {

    public ProjectShowByIndexResponse(@Nullable Project project) {
        super(project);
    }

}
