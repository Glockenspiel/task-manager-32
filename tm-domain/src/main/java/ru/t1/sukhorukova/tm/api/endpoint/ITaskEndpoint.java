package ru.t1.sukhorukova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.task.*;
import ru.t1.sukhorukova.tm.dto.response.task.*;

public interface ITaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskChangeStatusByIdResponse changeStatusById(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByIndexResponse changeStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request);

    @NotNull
    TaskClearResponse clear(@NotNull TaskClearRequest request);

    @NotNull
    TaskCompleteByIdResponse completeById(@NotNull TaskCompleteByIdRequest request);

    @NotNull
    TaskCompleteByIndexResponse completeByIndex(@NotNull TaskCompleteByIndexRequest request);

    @NotNull
    TaskCreateResponse create(@NotNull TaskCreateRequest request);

    @NotNull
    TaskListByProjectIdResponse listByProjectId(@NotNull TaskListByProjectIdRequest request);

    @NotNull
    TaskListResponse list(@NotNull TaskListRequest request);

    @NotNull
    TaskRemoveByIdResponse removeById(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByIndexResponse removeByIndex(@NotNull TaskRemoveByIndexRequest request);

    @NotNull
    TaskShowByIdResponse showById(@NotNull TaskShowByIdRequest request);

    @NotNull
    TaskShowByIndexResponse showByIndex(@NotNull TaskShowByIndexRequest request);

    @NotNull
    TaskStartByIdResponse startById(@NotNull TaskStartByIdRequest request);

    @NotNull
    TaskStartByIndexResponse startByIndex(@NotNull TaskStartByIndexRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskUpdateByIdResponse updateById(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByIndexResponse updateByIndex(@NotNull TaskUpdateByIndexRequest request);

}
