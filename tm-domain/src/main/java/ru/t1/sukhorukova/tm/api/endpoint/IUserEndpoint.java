package ru.t1.sukhorukova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.dto.request.user.*;
import ru.t1.sukhorukova.tm.dto.response.user.*;

public interface IUserEndpoint {

    @NotNull UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull UserLockResponse lock(@NotNull UserLockRequest request);

    @NotNull UserRegistryResponse registry(@NotNull UserRegistryRequest request);

    @NotNull UserRemoveResponse remove(@NotNull UserRemoveRequest request);

    @NotNull UserUnlockResponse unlock(@NotNull UserUnlockRequest request);

    @NotNull UserUpdateProfileResponse updateProfile(@NotNull UserUpdateProfileRequest request);

    @NotNull UserViewProfileResponse viewProfile(@NotNull UserViewProfileRequest request);

}
